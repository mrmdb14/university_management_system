#include<iostream>
#include <string.h>
using namespace std;
#define commandline() cout << ">> "
#define clear() printf("\033[H\033[J") // use clear(); to clean the terminal
#define studentD "/home/md14/CLionProjects/untitled8/students.txt"
#define coursesD "/home/md14/CLionProjects/untitled8/courses.txt"
typedef  struct infinite_str Str;
typedef  struct student Student;
typedef  struct course Course;
typedef  struct studentRecord StudentRecord;
//DS ----------------------------------------
struct infinite_str{
    char ch;
    Str* nextPtr;
};
struct course {
    char Course_id[8];
    char name[19];
    int Course_count;
    char professor[21];
};
struct  coursesRecord{
    Course product;
    int grad;
    coursesRecord* nPtr;
};
struct student {
    Str* name;
    Str* lastName;
    char id[7];
    coursesRecord* courses;
};
struct studentRecord{
    Student product;
    studentRecord* nPtr;
};
//functions----------------------
void  ReadFileSt(FILE* sfptr,Student& result);
coursesRecord* FReadCorse(FILE* fPtr);
int IsStudentInFile( char id[]) {
    FILE *PTR = fopen(studentD, "r");
    student result;
    while (fgetc(PTR) != EOF) {
        fseek(PTR, -sizeof(char), SEEK_CUR);
        // read a student
        ReadFileSt(PTR, result);
        if (!strcmp(result.id, id)) {
            fclose(PTR);
            return 1;
        }
    }
    fclose(PTR);
    return 0;

}
void FreeCourses( coursesRecord* head);
int IsThisCourseInFile(char Course_id[])
{
    FILE* fPtr= fopen(coursesD,"r");
    coursesRecord* head  = FReadCorse(fPtr);
    for(coursesRecord* i = head;i!= NULL;i= i->nPtr)
        if (!strcmp(Course_id, i->product.Course_id)) {
            FreeCourses(head);
            fclose(fPtr);
            return 1;
        }
    FreeCourses(head);
    fclose(fPtr);
    return 0;
}
void FreeStr(Str* head)
{

    Str* lptr=NULL;
    if(head == NULL)
        return;
    while(head != NULL)
    {
        lptr = head;
        head= head->nextPtr;
        free(lptr);
    }
}
void FreeCourses( coursesRecord* head)
{

    coursesRecord* lptr=NULL;
    if(head == NULL)
        return;
    while(head != NULL)
    {
        lptr = head;
        head= head->nPtr;
        free(lptr);
    }
}
void FreeStudent(StudentRecord* head)
{
    StudentRecord* lptr=NULL;
    Str* FREE;
    if(head == NULL)
        return;
    for(StudentRecord* i = head;  i!= NULL ;lptr = i ,i= i->nPtr) {
        FreeCourses(i->product.courses);
        }
    while(head != NULL)
    {
        lptr = head;
        head= head->nPtr;
        free(lptr);
    }
   /* for(StudentRecord* i = head;  i!= NULL ;lptr = i ,i= i->nPtr) {
        if (lptr != NULL) {
            free(lptr);
        }
    }
    */

}
void FreeOneStudent(StudentRecord* head)
{
    FreeStr(head->product.name);
    FreeStr(head->product.lastName);
    FreeCourses(head->product.courses);
    free(head);

}
void PressEnter() {

    char ch = '\0';
    cout<<"\t\t\t*Press ENTER key to Continue\n";
    commandline();
    //here also if you press any other key will wait till pressing ENTER
    while (ch != '\n')
        scanf("%c",&ch);
}
int IsSplit(char ch)
{
    if(ch == ' ')
        return 1;
    else if(ch == '\t')
        return 1;
    else if(ch == '\n')
        return 1;
    else
        return 0;
}
Str* ScanStr() {
    Str *head = NULL, *a = NULL, *Lprt = NULL;
    char ch = '\0';
    while (ch != '\n') {
        ch = char(getchar());
        a = (Str *) malloc(sizeof(Str));
        a->ch = ch;
        if (head == NULL)
            head = a;
        else
            Lprt->nextPtr = a;
        Lprt = a;
    }
    if (a != NULL)
        a->nextPtr = NULL;
    //remove Split
    if( head->nextPtr != NULL)
    for (a = head; a->nextPtr->nextPtr != NULL; a = a->nextPtr);
    free(a->nextPtr);
    a->nextPtr = NULL;
    return head;
}
coursesRecord* FScanCorse(FILE* fPtr)
{
    char ch = getc(fPtr);
    if (ch == '\n')
        return NULL;
    coursesRecord *add,*head=NULL,*lPtr=NULL;
    int i;
    char grad[3]={};
    while (ch == '(' )
    {
        i =0 ;
        grad[0]= '\0';
        add = (coursesRecord *)malloc(sizeof(coursesRecord));
        // BEGIN reading Course------------------------------------
        //get id------------------------
        add->product.Course_id[i] = getc(fPtr);
        i++;
        while(add->product.Course_id[i-1] != ',')
        {
            add->product.Course_id[i] = getc(fPtr);
            i++;
        }
        add->product.Course_id[i-1] = '\0';
        i = 0;
        //---------------------

        //get name------------------------
        while(add->product.name[i-1] != ',')
        {
            add->product.name[i] = getc(fPtr);
            i++;
        }
        add->product.name[i-1] = '\0';
        i = 0;
        //get count---------------------
        add->product.Course_count = getc(fPtr) - '0';
        ch = getc(fPtr); // ,
        //get professor ------------------------
        while(add->product.professor[i-1] != ',')
        {
            add->product.professor[i] = getc(fPtr);
            i++;
        }
        add->product.professor[i-1] = '\0';
        i=0;
        //get grade------------------------

        grad[i] = getc(fPtr);
        i++;
        while(grad[i-1] != ')')
        {
            grad[i] = getc(fPtr);
            i++;
        }
        add->grad = 0;
        grad[i-1] = '\0';
        for(int g= 0;grad[g]!='\0';g++)
        {
            add->grad *= 10;
            add->grad += (grad[g]-'0');
        }

        //END reading Course------------------------------------
        if(head == NULL)
            head = add;
        else
            lPtr-> nPtr = add;

        lPtr  = add;
        ch = getc(fPtr);
    }
    if(add!= NULL)
        add->nPtr= NULL;
    return head;
}
coursesRecord* FReadCorse(FILE* fPtr)
{

    char ch = getc(fPtr);
    coursesRecord *add,*head=NULL,*lPtr=NULL;
    int i;
    char grad[3]={};
    while (ch == '(' )
    {
        i =0 ;
        grad[0]= '\0';
        add = (coursesRecord *)malloc(sizeof(coursesRecord));
        // BEGIN reading Course------------------------------------
        //get id------------------------
        add->product.Course_id[i] = getc(fPtr);
        i++;
        while(add->product.Course_id[i-1] != ',')
        {
            add->product.Course_id[i] = getc(fPtr);
            i++;
        }
        add->product.Course_id[i-1] = '\0';
        i = 0;
        //---------------------

        //get name------------------------
        while(add->product.name[i-1] != ',')
        {
            add->product.name[i] = getc(fPtr);
            i++;
        }
        add->product.name[i-1] = '\0';
        i = 0;
        //get count---------------------
        add->product.Course_count = getc(fPtr) - '0';
        ch = getc(fPtr); // ,
        //get professor ------------------------
        while(add->product.professor[i-1] != ')')
        {
            add->product.professor[i] = getc(fPtr);
            i++;
        }
        add->product.professor[i-1] = '\0';
        i=0;

        //END reading Course------------------------------------
        if(head == NULL)
            head = add;
        else
            lPtr-> nPtr = add;

        lPtr  = add;
        ch = getc(fPtr);
    }
   /* if(add!= NULL)
         add->nPtr = NULL;
         */
    return head;
}
Str* FScanStr(FILE* fPtr) {
    Str *head = NULL, *a = NULL, *Lprt = NULL;
    char ch = '\0';
    while (ch != ',' && ch != ';') {
        ch = getc(fPtr);
        a = (Str *) malloc(sizeof(Str));
        a->ch = ch;
        if (head == NULL)
            head = a;
        else
            Lprt->nextPtr = a;
        Lprt = a;
    }
    if (a != NULL)
        a->nextPtr = NULL;
    //remove Split
    for (a = head; a->nextPtr->nextPtr != NULL; a = a->nextPtr);
    free(a->nextPtr);
    a->nextPtr = NULL;
    return head;
}
//---------- read a number in a certain range ----------------------------------------
int IsInRange(int key,int low,int high) {
    if (key < low)
        return 0;
    else if (key > high)
        return 0;
    return 1;

}
int  IsStrDigit(char in_str[]) {
    for (int i = 0; in_str[i] != '\0'; ++i)
        if (!isdigit(in_str[i]))
            return 0;
    return 1;
}
int ScanNum(int low,int high) {
    int result = 0;
    char str_number[100];
    int PrintState = 1;
    while ("number is not  in the range") {
        cin >> str_number;
        if (!IsStrDigit(str_number)) {
            if (PrintState) {
                cout << "invalid input" << endl;
                PrintState = 0;
            }
            commandline();
        } else {
            result = stoi(str_number);
            if (!IsInRange(result, low, high)) {
                if (PrintState) {
                    cout << "invalid input" << endl;
                    PrintState = 0;
                }
                commandline();
            } else
                break;
        }

    }
    getchar();
    return result;
}
//END--------------------------------------------------
// Print----------------------------------------------
void PrintHeaderLine(const char str[]) {
    int star=20;
    for (int i = 0; i <star ; ++i)
        printf("*");
    cout << str;
    for (int i = 0; i < star; ++i)
        printf("*");
    printf("\n");
}
void PrintMasterMenu() {
    clear();
    PrintHeaderLine("MAIN MENU");
    cout << "\t*Enter 1 to add student." << endl;
    cout << "\t*Enter 2 to add course for student." << endl;
    cout << "\t*Enter 3 to add course." << endl;
    cout << "\t*Enter 4 to see students sort by last name." << endl;
    cout << "\t*Enter 5 to see students sort by ID." << endl;
    cout << "\t*Enter 6 to see failed   students." << endl;
    cout << "\t*Enter 7 to see top-3    student by average." << endl;
    cout << "\t*Enter 8 to see students with the courses less then 14." << endl;
    cout << "\t*Enter 9 to remove student." << endl;
    cout << "\t*Enter 10 to see one student." << endl;
    cout << "\t*Enter 11 to remove  course." << endl;
    cout << "\t*Enter 12 to EXIT." << endl;


    commandline();
}
void PrintAddStudent()
{
    clear();
    PrintHeaderLine("ADD A STUDENT");
}
void PrintAddCoursesStudent()
{
    clear();
    PrintHeaderLine("ADD A Courses for Student");
}
void PrintAddCourse()
{
    clear();
    PrintHeaderLine("ADD A Courses");
}
//END------------------------------------------------------
void EXIT() {
    clear();
    cout << "have a nice day..."<<endl;
    // add delay
    exit(0);
}

void  addStudent_input(Student &add)
{
    int Error=0;
    // name
    PrintAddStudent();
    cout << "\t Pleas enter your first name."<<endl;
    commandline();
    add.name = ScanStr();
    // last name
    PrintAddStudent();
    cout << "\t Pleas enter your last name."<<endl;
    commandline();
    add.lastName = ScanStr();
    // id

    do {

        if(Error== 1)
        {
            cout << "\t this ID is already in file ." << endl;
            Error = -1;
        } else if (!Error) {//first time
            PrintAddStudent();
            cout << "\t Pleas enter your ID." << endl;
            Error = 1;
        }
    commandline();
    cin >> add.id;
    }while(IsStudentInFile(add.id));
    //-------------------files in here
}
void Sfprint(Student print)
{
    FILE* fPtr;
    fPtr = fopen(studentD, "r+");
    fseek(fPtr, 0, SEEK_END);
    for(Str* i = print.name;i!= NULL;i= i->nextPtr)
    {

    }
}
void addStudent(Student addOut,const char mod[10]) {
    FILE *fPtr;
    Str *i;
    Student add = {};
    fPtr = fopen(studentD, "r+");
    if (mod[0] == 'O') {
        add = addOut;
    } else {
        addStudent_input(add);
    }
    // Add add  to file
    fseek(fPtr, 0, SEEK_END);
    //add name
    for (i = add.name; i->nextPtr != NULL; i = i->nextPtr)
        fputc(i->ch, fPtr);
    if (i != NULL)
        fputc(i->ch, fPtr);
    fputc(',', fPtr);
    //add last name
    FreeStr(add.name);
    for (i = add.lastName; i->nextPtr != NULL; i = i->nextPtr)
        fputc(i->ch, fPtr);
    if (i != NULL)
        fputc(i->ch, fPtr);
    fputc(',', fPtr);
    FreeStr(add.lastName);
    //add id
    fputs(add.id, fPtr);
    fputc(';', fPtr);
    if (mod[0] == 'I') {
        fputc('\n', fPtr);
    }
    fclose(fPtr);
}
void  addCourse_input(Course &add)
{
    int Error=0;
    // id
    do {
        if(Error== 1)
        {
            cout << "\t this ID is already in file ." << endl;
            Error = -1;
        } else if (!Error) {//first time
            PrintAddCourse();
            cout << "\t Pleas enter ID of course."<<endl;
            Error = 1;
        }
        commandline();
        cin >> add.Course_id;
    }while(IsThisCourseInFile(add.Course_id));
    // name
    PrintAddStudent();
    cout << "\t Pleas enter name of the course  ."<<endl;
    commandline();
    cin >> add.name;
    // count
    PrintAddStudent();
    cout << "\t Pleas enter number of course."<<endl;
    commandline();
    cin >> add.Course_count;
    // count
    PrintAddStudent();
    cout << "\t Pleas enter professor of course."<<endl;
    commandline();
    cin >> add.professor;
}
void addCourse(Course Add, const char mod[])
{
    FILE *fPtr;
    fPtr = fopen(coursesD,"r+");
    Course add={};
    if(mod[0] == 'I')
     addCourse_input(add);
    else
        add = Add;
    fseek(fPtr,0,SEEK_END);
    fputc('(',fPtr);
    fputs(add.Course_id,fPtr);
    fputc(',',fPtr);
    fputs(add.name,fPtr);
    fputc(',',fPtr);
    fprintf(fPtr,"%d",add.Course_count);
    fputc(',',fPtr);
    fputs(add.professor,fPtr);
    fputc(')',fPtr);
    fclose(fPtr);
}
void FaddCourse(coursesRecord* add)
{
    FILE *fPtr= fopen(studentD,"r+");
    fseek(fPtr,0,SEEK_END);
    fputc('(',fPtr);
    fputs(add->product.Course_id,fPtr);
    fputc(',',fPtr);
    fputs(add->product.name,fPtr);
    fputc(',',fPtr);
    fprintf(fPtr,"%d",add->product.Course_count);
    fputc(',',fPtr);
    fputs(add->product.professor,fPtr);
    fputc(',',fPtr);
    fprintf(fPtr,"%d",add->grad);
    fputc(')',fPtr);
    fclose(fPtr);

}
void  ReadFileSt(FILE* sfptr,Student& result)
{


    char ch;
    result.name = FScanStr(sfptr);
    result.lastName = FScanStr(sfptr);
    fgets(result.id,7,sfptr);
    ch = getc(sfptr);// ;
    result.courses = FScanCorse(sfptr);
}
void  ReadFileStSHOW(FILE* sfptr,Student& result)
{

    FILE* PTR= fopen(studentD,"r");
    char ch;
    result.name = FScanStr(PTR);
    result.lastName = FScanStr(PTR);
    fgets(result.id,7,PTR);
    ch = getc(PTR);// ;
    result.courses = FScanCorse(PTR);
    fclose(PTR);
}

void  ReadFileCo(FILE* fptr,Course &found)
{
    char ch;
    int i =0;
    ch = getc(fptr);//(
    // id -----------------------------
    while (ch != ',' && ch != ';') {
        ch = getc(fptr);
        if(ch != ',')
            found.Course_id[i] = ch;
        i++;
    }
    found.Course_id[i-1] = '\0';
    i = 0;
    ch = '\0';
    // name --------------------------------
    while (ch != ',' && ch != ';') {
        ch = getc(fptr);
        if(ch != ',')
            found.name[i] = ch;
        i++;
    }
    found.name[i-1] = '\0';
    i = 0;
    ch = '\0';
    // count ------------------------------------
    found.Course_count  = getc(fptr)- '0';
    ch = getc(fptr);// ,
    ch = '\0';
    // professor --------------------------------
    while (ch != ')' && ch != ';') {
        ch = getc(fptr);
        if(ch != ')')
            found.professor[i] = ch;
        i++;
    }
    found.professor[i-1] = '\0';
}
void appendBN()
{
    FILE* fPtr = fopen(studentD,"r+");
    fseek(fPtr,0,SEEK_END);
    putc('\n',fPtr);
    fclose(fPtr);
}
StudentRecord* FindAndRemove(char id[],FILE* fPtr,Student& found) {
    //m
    FILE *trash;
    StudentRecord *head = NULL, *lPtr = NULL, *a;
    Student result = {};
    fPtr = fopen(studentD,"r+");
    rewind(fPtr);
    while (fgetc(fPtr)!= EOF) {
        fseek(fPtr,-sizeof(char),SEEK_CUR);
        ReadFileSt(fPtr, result);
        if (!strcmp(result.id, id)) {
            found = result;

        }
        else {
            a = (StudentRecord *) malloc(sizeof(StudentRecord));
            a->product = result;
            if (head == NULL)
                head = a;

            else
                lPtr->nPtr = a;

            lPtr = a;
        }
    }
    // empty file
    rewind(fPtr);
    trash = fopen(studentD, "w");
    fclose(trash);
    if(lPtr!= NULL)
        lPtr->nPtr= NULL;
    if (head == NULL)
        cout << "NO STUDENT YET...";
    else {
         // add students
        if (head!= NULL) {
            for (StudentRecord *i = head; i != NULL; i = i->nPtr) {
                addStudent(i->product, "OUT");
                // add courses of student
                fseek(fPtr, 0, SEEK_END);
                if (i->product.courses != NULL) {
                    for (coursesRecord *j = i->product.courses; j != NULL; j = j->nPtr) {
                        FaddCourse(j);
                    }
                }

                appendBN();
            }
        }


    }
    FreeStudent(head);
    fclose(fPtr);
}
Course FindCourse(FILE * fptr,char id[],int &FOUND)
{
    Course Found={};
    rewind(fptr);
    while (fgetc(fptr)!= EOF) {
        fseek(fptr,-sizeof(char),SEEK_CUR);

        ReadFileCo(fptr,Found);
        if(!strcmp(id,Found.Course_id)) {
            FOUND = 1;
            return Found;
        }
    }
    FOUND =0;
    return Found;
}
void addCourseStudent()
{
    int Error=0;
    char id[7];
    char Course_id[8];
    Student edit={};
    FILE *sfPtr;
    FILE *cfPtr;
    int FOUND=0;
    int grad;
    Course Found={};
    coursesRecord add={};
    sfPtr = fopen(studentD,"r+");
    cfPtr = fopen(coursesD,"r");
    PrintAddCoursesStudent();
    //FIND STUDENT------------------
    do {

        if(Error== 1)
        {
            cout << "\t this ID is not in the file." << endl;
            Error = -1;
        } else if (!Error) {//first time

            cout << "\t Pleas enter id of student" << endl;
            Error = 1;
        }
        commandline();
        cin >> id;
    }while(!IsStudentInFile(id));
    Error = 0;
    // save the student in memory and remove from file
    FindAndRemove(id,sfPtr,edit);
    fseek(sfPtr, -sizeof(char), SEEK_END);
    if(getc(sfPtr)!= '\n')
        appendBN();
    PrintAddCoursesStudent();
    //FIND Course------------------
    cout << "\t Pleas enter Course ID."<<endl;
    commandline();
    cin >> Course_id;
    do
    {
        Found  = FindCourse(cfPtr,Course_id,FOUND);
        if(!FOUND)
        {
            clear();
            PrintAddCoursesStudent();
            //NOT FOUND------------------
            cout << "\t NOT FOUND."<<endl;
            cout << "\t Pleas enter another Course ID."<<endl;
            commandline();
            cin >> Course_id;
        }
    }while (!FOUND);//Found is Found :|
    add.product = Found;
    fclose(cfPtr);
    //Read grad------------------
    cout << "\t Pleas enter grad of the Course."<<endl;
    commandline();
    cin >> grad;
    add.grad =grad;
    addStudent(edit,"OUT");
    for(coursesRecord* i = edit.courses;i!= NULL;i= i->nPtr)
    {
        fseek(sfPtr,0,SEEK_END);
        FaddCourse(i);
    }
    FaddCourse(&add);
    appendBN();
    fclose(sfPtr);
}
float avg (student result)
{
    int sum=0;
    int sumCount=0;
    if (result.courses== NULL)
        return 0;
    for(coursesRecord* i=result.courses;i!=NULL;i=i->nPtr)
    {
        sum+=i->grad*(i->product.Course_count);
        sumCount+=(i->product.Course_count);
    }
    return  sum/sumCount;
}
int SigmaCount(Student result)
{

    int sum = 0;
    if (result.courses == NULL)
        return 0;
    for(coursesRecord* i = result.courses;i != NULL ;i= i->nPtr)
        sum += i->product.Course_count;
    return  sum;
}

int  strCmp(Str* s1,Str* s2){// or int IsS1Bigger
    Str*  a2=s2;
    for(Str* a1=s1;a1!=NULL && a2 != NULL;a1=a1->nextPtr)
    {
        if(a1->ch>a2->ch)
            return 1;
        else if (a1->ch !=a2->ch)
            return 0;
        a2= a2->nextPtr;
    }
}
void PrintStr(Str* head)
{
    for(Str* i = head;i!= NULL;i= i->nextPtr)
        cout << i->ch;
}
void PrintStudent(int func,StudentRecord* head)
{
    StudentRecord* free=NULL;
    clear();
    switch (func) {
        case 4: {
            PrintHeaderLine(" All Students_sort by last name");
            break;
        }
        case 5: {
            PrintHeaderLine("All Students_sort by ID");
            break;
        }
        case 6: {
            PrintHeaderLine(" failed student.");
                break;
        }
        case 7: {
            PrintHeaderLine("top 3 by average.");
            break;
        }
        case 8: {
            PrintHeaderLine("passed less then 14 Course.");
                break;
        }
        case 10: {
            PrintHeaderLine("student.");
            break;
        }
        default:
            cout << "ERROR";
            exit(0);
    }
    cout <<"\tname\t"<<"last name\t"<<"ID\t\t\t"<<"average"<<endl;

    for(StudentRecord* i = head;i!= NULL;i= i->nPtr)
    {
        cout<<"\t";
        PrintStr(i->product.name);
        cout<<"\t\t";
        PrintStr(i->product.lastName);
        cout<<"\t\t\t"<<i->product.id<<"\t\t";
        if(avg(i->product))
            cout << avg(i->product)<<endl;
        else
            cout << "NO COURSE YET!!!"<<endl;
        if(free!= NULL)
        FreeOneStudent(free);
        free = i;

    }

    PressEnter();
    if(func== 10)
        getchar();
    clear();
}
void PRemoveStudent()
{
    clear();
    PrintHeaderLine("Remove a student");
    cout << "\t Ensert an id to remove student."<<endl;
    commandline();
}
void PShowStudent()
{
    clear();
    PrintHeaderLine("Show a student");
    cout << "\t Ensert an id to Show student."<<endl;
    commandline();
}
void PRemoveCourse()
{
    clear();
    PrintHeaderLine("remove a course");
    commandline();
}
int CharStarcmp(char s1[],char s2[])
{
    for(int i =0;s1[i]!= '\0' && s2[i]!= '\0';++i)
    {
        if(s1[i]>s2[i])
            return +1;
        else if (s1[i]!= s2[i])
            return -1;
    }
    return 0;
}
void  showFunctions(int func) {
    FILE *PTR;
    char ch;
    int state = 0;
    StudentRecord f={};
    PTR = fopen(studentD, "r");
    StudentRecord *head = NULL, *a=NULL, *lastPtr,*final;
    student result = {};
    StudentRecord *i;
    rewind(PTR);
    while ((ch = fgetc(PTR)) != EOF) {
        fseek(PTR, -sizeof(char), SEEK_CUR);
        // read a student
        ReadFileSt(PTR, result);

        // check the student for each function.
        switch (func) {
            case 4: {
                //none
                break;
            }
            case 5: {
                //none
                break;
            }
            case 6: {
                if (avg(result) > 12)
                    continue;
                else
                    break;
            }
            case 7: {
                //none
                break;
            }
            case 8: {
                if (SigmaCount(result) > 14)
                    continue;
                else
                    break;
            }
            default:
                cout << "ERROR";
                exit(0);
        }
        a = (StudentRecord *) malloc(sizeof(StudentRecord));
        a->product = result;
        a->nPtr = NULL;
        if (head == NULL)
            head = a;
        else {
            switch (func) {
                default: {
                    cout << "ERROR";
                    exit(0);
                }
                case 4: {
                    for (i = head, lastPtr = NULL;
                         i != NULL && strCmp(result.lastName, i->product.lastName); lastPtr = i, i = i->nPtr);
                    break;
                }
                case 5: {

                    for (i = head, lastPtr = NULL;
                         i != NULL&& CharStarcmp(i->product.id, result.id) < 0; lastPtr = i, i = i->nPtr);
                    break;
                }
                case 6: {
                    i = head;// add to the first of the list
                    break;
                }
                case 7: {
                    for (i = head, lastPtr = NULL;
                         i != NULL && avg(result) < avg(i->product); lastPtr = i, i = i->nPtr);
                    break;
                }
                case 8: {
                    i = head;// add to the first of the list
                    break;
                }
            }
            if (i == head) {
                a->nPtr = head;
                head = a;
                final = head;
            } else if (i == NULL) {

                lastPtr->nPtr = a;
                a->nPtr = NULL;
                final  = a;
            } else {

                a->nPtr = i;
                lastPtr->nPtr = a;


            }
        }

    }

    if (func == 7) {
        a  = head;
        for(int i = 0 ;a!= NULL&&i<2;++i) {
            a = a->nPtr;
        }
        //FREE
        if(a!=  NULL)
        a->nPtr= NULL;
    }
    PrintStudent(func, head);
    fclose(PTR);
}
void  RemoveStudent()
{
    Student trash;
    FILE* fPtr= fopen(studentD,"r+");
    char id[7];
    PRemoveStudent();
    cin >> id;
    FindAndRemove(id,fPtr,trash);
}
void ShowStudent() {

    StudentRecord result = {};
    FILE *PTR = fopen(studentD, "r+");
    char id[7];
    PShowStudent();
    cin >> id;
    while (fgetc(PTR) != EOF) {
        fseek(PTR, -sizeof(char), SEEK_CUR);
        // read a student
        ReadFileSt(PTR, result.product);
        if (!CharStarcmp(result.product.id, id)) {
            PrintStudent(10, &result);
        }
    }
    fclose(PTR);

}

coursesRecord* FAndRCoufromll(coursesRecord* &head,char Course_id[])// find and remove course from link list
{
    coursesRecord* found=NULL,*lastPtr=NULL;
    for (found = head,lastPtr= NULL; found != NULL &&
                                     CharStarcmp(found->product.Course_id,Course_id);lastPtr = found, found = found->nPtr);

    if(found != NULL)
    {
        if(lastPtr == NULL)
        {
            head = head->nPtr;

        } else
            lastPtr->nPtr = found ->nPtr;
    }
    return found;
}
void RemoveCourseFromStudent( char Course_id[])
{
    StudentRecord* haed = NULL,*a,*lastPtr;
    StudentRecord result = {};
    FILE *trash;
    FILE *PTR = fopen(studentD, "r+");
    coursesRecord* cLastPtr=NULL,*i;
    while (fgetc(PTR) != EOF) {
        fseek(PTR, -sizeof(char), SEEK_CUR);
        // read a student

        ReadFileSt(PTR, result.product);
        // find and remove course

        //-----------------------
        a = (StudentRecord*) malloc(sizeof(StudentRecord));


        a->product  = result.product;

        if( haed == NULL)
            haed  = a;
        else
            lastPtr->nPtr = a;
        lastPtr= a;
    }
    trash = fopen(studentD, "w");
    fclose(trash);
    for(a = haed;a!= NULL;a= a->nPtr)
    {
        addStudent(a->product,"OUT");
        if (a->product.courses!= NULL) {
            for (i = a->product.courses; i != NULL; i = i->nPtr) {
                if (CharStarcmp(i->product.Course_id, Course_id)) {
                    FaddCourse(i);
                }
            }
        }

    appendBN();
    }
    fclose(PTR);
    FreeStudent(haed);
}
void RemoveCourse() {
    FILE *fptr = fopen(coursesD, "r+");
    char Course_id[8];
    coursesRecord *head, *found,*lastPtr;
    PRemoveCourse();
    int Error=0;
    // id
    do {
        if(Error== 1)
        {
            cout << "\t this ID is not in file ." << endl;
            Error = -1;
        } else if (!Error) {//first time
            PrintAddCourse();
            cout << "\t Pleas enter ID of course."<<endl;
            Error = 1;
        }
        commandline();
        cin >> Course_id;
    }while(!IsThisCourseInFile(Course_id));
    head = FReadCorse(fptr);
    //find course------------------------------
    FAndRCoufromll(head,Course_id);
    fclose(fptr);
    fopen(coursesD,"w");
    fclose(fptr);
    for (coursesRecord* add = head;add != NULL ;add = add->nPtr) {
    addCourse(add->product,"OUT");
    }
    //  removed from Courses.txt
    RemoveCourseFromStudent(Course_id);

}
int MasterMenu() {
    Student trash = {};
    course Trash={};
    int result;
    while ("True") {
        PrintMasterMenu();
        result = ScanNum(1, 12);
        switch (result) {
            case 1:
                addStudent(trash, "IN");
                break;
            case 2:
                addCourseStudent();
                break;
            case 3:
                addCourse(Trash,"IN");
                break;
            case 4:
                showFunctions(4);
                break;
            case 5:
                showFunctions(5);
                break;
            case 6:
                showFunctions(6);
                break;
            case 7:
                showFunctions(7);
                break;
            case 8:
                showFunctions(8);
                break;
            case 9:
                RemoveStudent();
                break;
            case 10:
                ShowStudent();
                break;
            case 11:
                RemoveCourse();
                break;
            default:
                clear();
                cout << "An unknown Error has occurred :(" << endl << "EXITING....";
            case 12:
                EXIT();
        }//end switch
    }//end while
}
void WellCome() {
    clear();
    cout << endl;
    cout << "\t\tÛÛÛÛÛ  ÛÛÛÛÛ    ÛÛÛÛÛÛ   ÛÛÛÛÛÛ     ÛÛÛÛÛÛÛÛÛ" << endl;
    cout << "\t\t°ÛÛÛ  °°ÛÛÛ    °°ÛÛÛÛÛÛ ÛÛÛÛÛÛ     ÛÛÛ°°°°°ÛÛÛ" << endl;
    cout << "\t\t°ÛÛÛ   °ÛÛÛ     °ÛÛÛ°ÛÛÛÛÛ°ÛÛÛ    °ÛÛÛ    °°°" << endl;
    cout << "\t\t°ÛÛÛ   °ÛÛÛ     °ÛÛÛ°°ÛÛÛ °ÛÛÛ    °°ÛÛÛÛÛÛÛÛÛ" << endl;
    cout << "\t\t°ÛÛÛ   °ÛÛÛ     °ÛÛÛ °°°  °ÛÛÛ     °°°°°°°°ÛÛÛ" << endl;
    cout << "\t\t°ÛÛÛ   °ÛÛÛ     °ÛÛÛ      °ÛÛÛ     ÛÛÛ    °ÛÛÛ" << endl;
    cout << "\t\t°°ÛÛÛÛÛÛÛÛ   ÛÛ ÛÛÛÛÛ     ÛÛÛÛÛ ÛÛ°°ÛÛÛÛÛÛÛÛÛ  ÛÛ" << endl;
    cout << "\t\t°°°°°°°°   °° °°°°°     °°°°° °°  °°°°°°°°°  °°" << endl;
    cout << "\t\t\tuniversity management system" << endl<< endl;

    PressEnter();
    clear();

}
void newDatabase()
{
    FILE *trash = fopen(studentD, "w");
    fclose(trash);
    trash = fopen(coursesD, "w");
    fclose(trash);
    cout << "new data bases is located in  :"<<endl;
    cout << "students :"<<studentD<<endl;
    cout << "courses :"<<coursesD<<endl;
    exit(0);

}
int main() {
#if defined(_WIN32) || defined(_WIN64)
    system("color");
#endif
    newDatabase();
    WellCome();
    MasterMenu();

}
